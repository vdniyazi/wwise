/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AMB_DESERT = 3346050181U;
        static const AkUniqueID FOOTSTEPS = 2385628198U;
    } // namespace EVENTS

    namespace SWITCHES
    {
        namespace SWITCH_LOCOMOTION_TYPE
        {
            static const AkUniqueID GROUP = 3794968246U;

            namespace SWITCH
            {
                static const AkUniqueID SWITCH_RUN = 3818208631U;
                static const AkUniqueID SWITCH_WALK = 3627811739U;
            } // namespace SWITCH
        } // namespace SWITCH_LOCOMOTION_TYPE

        namespace SWITCH_SURFACE_TYPE
        {
            static const AkUniqueID GROUP = 2818941756U;

            namespace SWITCH
            {
                static const AkUniqueID SWITCH_SURFACE_CONCRETE = 3294452117U;
                static const AkUniqueID SWITCH_SURFACE_SAND = 3129684586U;
            } // namespace SWITCH
        } // namespace SWITCH_SURFACE_TYPE

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID DENSITY_FA_SSGRAIN = 2715217995U;
        static const AkUniqueID EXT_FOOT_VELOCTIY = 3990589487U;
        static const AkUniqueID IGN_ENG_EXH_POSITION = 1776289502U;
        static const AkUniqueID IGN_GEARBOX_THROTTLE = 3744760799U;
        static const AkUniqueID IGN_HARLEY_GRAIN_LOOP_BLEND = 1573260114U;
        static const AkUniqueID IGN_LOAD = 1046277832U;
        static const AkUniqueID IGN_RPM = 2202868459U;
        static const AkUniqueID IGNITERSYNTH_REV = 1355046805U;
        static const AkUniqueID IGNITERSYNTH_STATE = 3696491809U;
        static const AkUniqueID IMMERSION_FA_SSGRAIN = 2481728872U;
        static const AkUniqueID INSTRUMENT_FA_SSGRAIN = 2317409760U;
        static const AkUniqueID PLAYBACK_RATE = 1524500807U;
        static const AkUniqueID PROXIMITY_FA_SSGRAIN = 1791284502U;
        static const AkUniqueID RPM = 796049864U;
        static const AkUniqueID RPM_FA_SSGRAIN = 1656280998U;
        static const AkUniqueID SIMULATION_FA_SSGRAIN = 2428833394U;
        static const AkUniqueID SS_AIR_FEAR = 1351367891U;
        static const AkUniqueID SS_AIR_FREEFALL = 3002758120U;
        static const AkUniqueID SS_AIR_FURY = 1029930033U;
        static const AkUniqueID SS_AIR_MONTH = 2648548617U;
        static const AkUniqueID SS_AIR_PRESENCE = 3847924954U;
        static const AkUniqueID SS_AIR_RPM = 822163944U;
        static const AkUniqueID SS_AIR_SIZE = 3074696722U;
        static const AkUniqueID SS_AIR_STORM = 3715662592U;
        static const AkUniqueID SS_AIR_TIMEOFDAY = 3203397129U;
        static const AkUniqueID SS_AIR_TURBULENCE = 4160247818U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MASTER = 4056684167U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMBIENCE = 85412153U;
        static const AkUniqueID CHARACTER = 436743010U;
        static const AkUniqueID INDOOR = 340398852U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MOTION_FACTORY_BUS = 985987111U;
        static const AkUniqueID OUTDOOR = 144697359U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID DEFAULT_MOTION_DEVICE = 4230635974U;
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
