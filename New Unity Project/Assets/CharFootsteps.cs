﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class CharFootsteps : MonoBehaviour
{
    public AK.Wwise.Event footstepsevent;
    vThirdPersonInput   tpinput;
    public LayerMask lm;
    string surface;
    public GameObject FootLeft;
    public GameObject FootRight;
    public Animator PlayerAnimator;
    bool leftIsPlaying;
    bool rightIsPlaying;

    // Start is called before the first frame update
    void Start()
    {
        tpinput = gameObject.GetComponent<vThirdPersonInput>();
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerAnimator.GetFloat("foot_left") > 0.01 && !leftIsPlaying)
        {
            Footsteps();
            leftIsPlaying = true;
        }
        else if (PlayerAnimator.GetFloat("foot_left") == 0) leftIsPlaying = false;


            if (PlayerAnimator.GetFloat("foot_right") > 0.01 && !rightIsPlaying)
        { 
            Footsteps();
            rightIsPlaying = true;
        }

        else if (PlayerAnimator.GetFloat("foot_right") == 0) rightIsPlaying = false;


    }

    void Footsteps()
    {

            SurfaceCheck();

            if (tpinput.cc.inputMagnitude > 1) 
            {
                AkSoundEngine.SetSwitch("SWITCH_locomotion_type","SWITCH_run",gameObject);
                AkSoundEngine.SetSwitch("SWITCH_surface_type", surface, gameObject);
                footstepsevent.Post(gameObject); 
            }

            else
            {
                AkSoundEngine.SetSwitch("SWITCH_locomotion_type","SWITCH_walk", gameObject); 
                AkSoundEngine.SetSwitch("SWITCH_surface_type", surface, gameObject);
                footstepsevent.Post(gameObject);
            }
        

    }
    
    void SurfaceCheck()
    {
        if (Physics.Raycast(gameObject.transform.position, Vector3.down, out RaycastHit hit, 1f, lm))
        {
            surface = hit.collider.tag;
        }
    }

}
